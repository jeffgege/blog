+++
date = "2019-11-30"
+++

Hi, my name is Jeff Gegetskas. I am a student at University of Advancing Technology in their Network Security program. I also currently work at Quality Health Ideas, Inc. as a Systems Support Specialist. I have experience in a wide verity of IT topics. I have recently started working on a lot of Python projects. This includes automation of daily tasks and converting PHP application into Python Flask applications. While going to school for Network security I also enjoy researching and building those skills in my free time. Though I try not to limit myself to one technology I hope this blog can be about a number of different subjects from programming to security to server administration topics.

While I am still looking for a way to add comments to my blog you can always reach out to me through any of the social media links on my home page. You can also email me at [jgegetskas@gmail.com](mailto:jgegetskas@gmail.com) with any questions or project requests. 
