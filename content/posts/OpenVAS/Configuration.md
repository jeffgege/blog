+++
date = "2019-12-23"
title="OpenVAS - Configuration"
draft= true
+++

Now that we have our OpenVAS instance setup. I forgot to include the automation of updating the secinfo in the last article, so lets do that now. This is a very simple list of commands in a script and I hope to update with some error checking, but here it is. First you are going to create a file called update-openvas in a folder that's in your PATH variable. You can check this by typing ```echo $PATH```. I put my script in /usr/local/bin. So I would do ```nano /usr/local/bin/update-openvas```. Then type or paste the followiing commands in that file.

```
/usr/sbin/greenbone-nvt-sync
/usr/sbin/greenbone-certdata-sync
/usr/sbin/greenbone-scapdata-sync
/usr/sbin/openvasmd -update -verbose -progress
/etc/init.d/openvas-manager restart
/etc/init.d/openvas-scanner restart
```
Then save and close the file. You should now be able to run the command ```update-openvas``` and it should update all the secinfo and restart the required services. You can also add this to crontab and have it run on a schedule.

Next, we are going to setup some hosts in OpenVAS and configure a couple scans. First login to your OpenVAS web console and hover over configuration and then click targets. Right above the target in the top right part of the page you'll see a page with a star click on that to launch the create target wizard. Then enter the name of the target. Since I only have a few hosts to scan I create them individually, but you can create a target for a subnet as well if you want to scan multiple hosts with one target. So for my name I'll enter the hostname of the target and enter the IP address for the host. I then leave all the other options set to default and click save. Do this for all the hosts you will be scanning.
