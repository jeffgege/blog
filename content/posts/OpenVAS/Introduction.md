+++
date = "2019-12-17"
title="OpenVAS - Introduction"
+++

Recently I've been testing out OpenVAS also known as Greenbone Security Assistant. and one thing I've noticed is that the reporting capabilities are very basic. Now, before I get too far I do want to say that I think that it is a great project. I think it does a great job at scanning devices for known vulnerabilities and it is really easy to maintain. I've noticed that once you get it set up and configured it's a set it and forget it for the most part. It also has a great and graphic dashboard. The one thing that I think that it is lacking is the reports you can output with it. I decided to see what I could do to make them look a bit better and provide more of an executive-level overview along with more in-depth content as well.

So for this project, I'm going to start by first, installing OpenVAS. This is where I go through the server configuration as well. Such as automating the secinfo used in OpenVAS. Next, I'll go over setting up hosts and scans. This won't go too far in-depth since there are articles out there that will have more detail as I'm just going to be setting simple initial scans that won't get too granular. Then we'll dive into creating something to convert the reports into something more useful. I plan to be writing the tool in Python most likely using Pandas and some other tools. There will be more on the more specific process when we get there.
