+++
date = "2019-12-02"
title = "Hosting a Site on Gitlab - Introduction"
+++
In this series I'm going to explain how I created this blog. I will go over setting up the hosting on [Gitlab](https://about.gitlab.com/) and DNS configuration. Then I will discuss setting up you're first static site using [Hugo](https://gohugo.io/). I plan on keeping the articles short for the most part to hopefully make it easier to work through.

In this series I will be using PopOS which is Ubuntu based as well as Hugo v0.57.2. Some steps may change based on your enviroment, but I plan to keep this as up to date as possible. If you have any questions, comments, or fixes for this series please let me know by sending me an email to [jgegetskas@gmail.com](mainto:jgegetskas@gmail.com).
